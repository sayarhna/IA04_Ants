package model;

import sim.engine.SimState;
import sim.engine.Stoppable;
import sim.field.grid.SparseGrid2D;
import sim.util.Bag;
import sim.util.Int2D;

public class Beings extends SimState {
	public SparseGrid2D yard = new SparseGrid2D(Constants.GRID_SIZE, Constants.GRID_SIZE);
	
	private int numInsects = 0;
		public int getNumInsects () { return numInsects; }
		public void decrementNumInsects () { this.numInsects--; }

	public Beings (long seed) {
		super(seed);
	}

	public void start () {
		super.start();
		yard.clear();
		addInsects();
		addFood();
	}

	private void addInsects () {
		for (int i = 0; i < Constants.NUM_INSECT; i++) {
			Insect insect = new Insect(this);
			Stoppable stoppable = schedule.scheduleRepeating(insect);
			insect.setStoppable(stoppable);
			
			Int2D location = getFreeLocation();
			yard.setObjectLocation(insect, location);
			insect.x = location.x;
			insect.y = location.y;
			numInsects++;
		}
	}

	private void addFood () {
		for (int i = 0; i < Constants.NUM_FOOD_CELL; i++) {
			addFoodSource();
		}
	}

	private void addFoodSource () {
		Int2D location = getFreeLocation();
		int foodQtt = 1 + random.nextInt(Constants.MAX_FOOD);
		for (int j = 0; j < foodQtt; j++) {
			Food food = new Food();
			yard.setObjectLocation(food, location);
			food.x = location.x;
			food.y = location.y;
		}
	}
	
	private Int2D getFreeLocation () {
		Int2D location = null;
		boolean freeLocationFound = false;
		while (!freeLocationFound) {
			location = new Int2D(random.nextInt(yard.getWidth()), random.nextInt(yard.getHeight()));
			Bag bag = yard.getObjectsAtLocation(location);
			if (bag == null) { return location; }

			freeLocationFound = true;
			Object[] agents = bag.objs;
			for (Object ag : agents) {
				if (ag != null) {
					freeLocationFound = false;
				}
			}
		}
		return location;
	}

	public void removeFood (Food food) {
		Int2D location = yard.getObjectLocation(food);
		yard.remove(food);
		boolean stillHasFood = false;
		Bag remainingObjects = yard.getObjectsAtLocation(location);
		if (remainingObjects != null) {
			Object[] remainingAgents = remainingObjects.objs;
			for (Object ag : remainingAgents) {
				if (ag != null && ag.getClass() == Food.class) {
					stillHasFood = true;
				}
			}
		}
		if (!stillHasFood) {
			addFoodSource();
		}
	}
}
