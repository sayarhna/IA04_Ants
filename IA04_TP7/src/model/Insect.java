package model;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import sim.util.Bag;
import sim.util.Int2D;

public class Insect extends AgentType implements Steppable {
	
	private final int perception;
	private final int maximumWeight;
	private final int movement;
	
	private int energy = Constants.MAX_ENERGY;
	private int carriedFood = 0;
	private Stoppable stoppable;
	
	public Insect (Beings beings) {
		int caracteristicPoints = Constants.CAPACITY;
		perception = 2 + beings.random.nextInt(caracteristicPoints);
		caracteristicPoints -= perception - 1;
		maximumWeight = 2 + (caracteristicPoints > 0 ? beings.random.nextInt(caracteristicPoints) : -1);
		caracteristicPoints -= maximumWeight - 1;
		movement = 1 + caracteristicPoints;
	}

	@Override
	public void step (SimState state) {
		Beings beings = (Beings) state;
		if (energy == 0) {
			beings.yard.remove(this);
			stoppable.stop();
			beings.decrementNumInsects();
			return;
		}
		action(beings);
	}

	public void action (Beings beings) {
		if (energy + Constants.FOOD_ENERGY <= Constants.MAX_ENERGY) {
			Food food = nextToFood(beings);
			if (food != null) {
				eatFood(beings, food);
				return;
			}
			if (carriedFood > 0) {
				eatCarriedFood();
				return;
			}
		}
		
		if (carriedFood <= maximumWeight) {
			Food food = food(beings, x, y);
			if (food != null) {
				pickFood(beings, food);
				return;
			}
		}

		move(beings);
	}
	
	public void eatFood (Beings beings, Food food) {
		beings.removeFood(food);
		energy += Constants.FOOD_ENERGY;
	}
	
	public void pickFood (Beings beings, Food food) {
		beings.removeFood(food);
		carriedFood++;
	}
	
	public void eatCarriedFood () {
		energy += Constants.FOOD_ENERGY;
		carriedFood--;
	}
	
	public void move (Beings beings) {
		Int2D foodLocation = seenFoodLocation(beings);
		int n = beings.random.nextInt(Constants.NB_DIRECTIONS);
		
		for (int i = 0; i < movement; i++) {
			if (foodLocation != null) {
				if (foodLocation.x < x) {
					if (foodLocation.y > y) {
						n = 6;
					} else if (foodLocation.y < y) {
						n = 4;
					} else {
						n = 0;
					}
				} else if (foodLocation.x > x) {
					if (foodLocation.y > y) {
						n = 7;
					} else if (foodLocation.y < y) {
						n = 5;
					} else {
						n = 1;
					}
				} else {
					if (foodLocation.y > y) {
						n = 3;
					} else if (foodLocation.y < y) {
						n = 2;
					} else {
						// La nourriture est la fourmi sont sur la m�me case : pas normal
						n = -1;
					}
				}
			}
			
			switch (n) {
				case 0:
					beings.yard.setObjectLocation(this, beings.yard.stx(x - 1), y);
					x = beings.yard.stx(x - 1);
					break;
				case 1:
					beings.yard.setObjectLocation(this, beings.yard.stx(x + 1), y);
					x = beings.yard.stx(x + 1);
					break;
				case 2:
					beings.yard.setObjectLocation(this, x, beings.yard.sty(y - 1));
					y = beings.yard.sty(y - 1);
					break;
				case 3:
					beings.yard.setObjectLocation(this, x, beings.yard.sty(y + 1));
					y = beings.yard.sty(y + 1);
					break;
				case 4:
					beings.yard.setObjectLocation(this, beings.yard.stx(x - 1), beings.yard.sty(y - 1));
					x = beings.yard.stx(x - 1);
					y = beings.yard.sty(y - 1);
					break;
				case 5:
					beings.yard.setObjectLocation(this, beings.yard.stx(x + 1), beings.yard.sty(y - 1));
					x = beings.yard.stx(x + 1);
					y = beings.yard.sty(y - 1);
					break;
				case 6:
					beings.yard.setObjectLocation(this, beings.yard.stx(x + 1), beings.yard.sty(y + 1));
					x = beings.yard.stx(x + 1);
					y = beings.yard.sty(y + 1);
					break;
				case 7:
					beings.yard.setObjectLocation(this, beings.yard.stx(x - 1), beings.yard.sty(y + 1));
					x = beings.yard.stx(x - 1);
					y = beings.yard.sty(y + 1);
					break;
			}
			if ((energy + Constants.FOOD_ENERGY <= Constants.MAX_ENERGY && nextToFood(beings) != null)
					|| (carriedFood <= maximumWeight && food(beings, x, y) != null)) {
				break;
			}

			if (foodLocation == null) {
				foodLocation = seenFoodLocation(beings);
			}
		}
		energy--;
	}
	
	private Food food (Beings beings, int x, int y) {
		Int2D location = new Int2D(beings.yard.stx(x), beings.yard.sty(y));
		Bag objects = beings.yard.getObjectsAtLocation(location);
		if (objects == null) {
			return null;
		}
		
		Object[] agents = objects.objs;
		for (Object ag : agents) {
			if (ag != null && ag.getClass() == Food.class) {
				return (Food) ag;
			}
		}
		return null;
	}

	protected Food nextToFood (Beings beings) {
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				if (i != 0 || j != 0) {
					Food food = food(beings, x + i, y + j);
					if (food != null) {
						return food;
					}
				}
			}
		}
		return null;
	}
	
	protected Int2D seenFoodLocation (Beings beings) {
		Int2D foodLocation = null;
		for (int i = 1; i <= perception && foodLocation == null; i++) {
			foodLocation = getFoodLocationAtDistance(beings, i);
		}
		return foodLocation;
	}
	
	protected Int2D getFoodLocationAtDistance (Beings beings, int distance) {
		for (int i = -distance; i <= distance; i++) {
			for (int j = -distance; j <= distance; j++) {
				if (Math.abs(i) == distance || Math.abs(j) == distance) {
					Int2D flocation = new Int2D(beings.yard.stx(x + i), beings.yard.sty(y + j));
					Bag objects = beings.yard.getObjectsAtLocation(flocation);
					if (objects != null) {
						Object[] agents = objects.objs;
						for (Object ag : agents) {
							if (ag != null && ag.getClass() == Food.class) {
								return flocation;
							}
						}
					}
				}
			}
		}
		return null;
	}

	public void setStoppable(Stoppable stoppable) {
		this.stoppable = stoppable;
	}
}
