package model;

public class Constants {
	public static int GRID_SIZE = 30;
	public static int NB_DIRECTIONS = 8;
	public static int NUM_INSECT = 10;
	public static int NUM_FOOD_CELL = 15;
	public static int MAX_ENERGY = 15;
	public static int MAX_LOAD = 5;
	public static int CAPACITY = 10;
	public static int MAX_FOOD = 5;
	public static int FOOD_ENERGY = 3;
}
